//part 1
// const circle  = {
//     radius:1,
//     location:{
//         x:1,
//         y:1
//     },
//     draw:function(){
//         console.log('draw');
//     }//method
// };//object literal syntax
// circle.draw();

//part 2

// //factory
// function createCircle(radius){
//     return{
//         radius,
//         draw:function(){
//             console.log('draw')
//         }
//     };
// }

// const circle =createCircle(1);
// circle.draw();

// //part 3

// //constructor
// function Circle(radius){
// // console.log('this',this);
//     this.radius = radius;
//     this.draw = function(){
//         console.log('draw');
//     }
// }

// const another = new Circle(2);
// console.log(another);

//part 4
function Circle(radius){
    // console.log('this',this);
        this.radius = radius;
        this.draw = function(){
            console.log('draw');
        }
    }
    const another = new Circle(2);